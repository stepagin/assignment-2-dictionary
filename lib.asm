; lib.asm
section .text
 
 
; Принимает код возврата и завершает текущий процесс
global exit
exit: 
	; код ошибки - в rdi
    xor rax, rax, ; обнуление аккумулятора
	mov rax, 60, ; запись в аккумулятор кода выхода
	syscall ; вызов кода выхода

global print_error
print_error:
    xor rax, rax
	call string_length
	mov rdx, rax ; количество символов в rdx
	mov rsi, rdi ; указатель на начало строки в rsi
	mov rax, 1 ; команда sys_write
	mov rdi, 2 ; вывод - в stderr
	syscall	; вызываем sys_write
    ret
	



; Принимает указатель на нуль-терминированную строку, возвращает её длину
global string_length
string_length:
    xor rax, rax
	.loop:
		cmp byte [rdi+rax], 0 ; проверка, что нуль-терминированная 
		je .return            ; строка не закончилась
		inc rax               ; следующий символ
		jmp .loop            ; продолжить чтение
	.return:                  ; возврат
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
global print_string
print_string:
    xor rax, rax
	call string_length
	mov rdx, rax ; количество символов в rdx
	mov rsi, rdi ; указатель на начало строки в rsi
	mov rax, 1 ; команда sys_write
	mov rdi, 1 ; вывод - в stdout
	syscall	; вызываем sys_write
    ret

; Принимает код символа и выводит его в stdout
global print_char
print_char:
	push rdi ; записываем char в стек
	xor rdi, rdi ; обнуляем регистр, чтобы вписать в него stdout
    xor rax, rax
	mov rdi, 1 ; вывод - в stdout
	mov rax, 1 ; команда sys_write
	mov rsi, rsp ; в rsi записываем символ
	mov rdx, 1 ; длина вывода - 1
	
	syscall
	pop rdi ; возвращаем стек
    ret

; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
    mov rdi, 0xA ; запись символа переноса строки в rdi
	call print_char ; вызов функции напечатать символ
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
global print_uint
print_uint:
    xor rax, rax
	push rbx ; сохраняем значение rbx
    mov rbx, 10 ; основание СС
	
    dec rsp ; указатель стека -1
    mov byte [rsp], 0 ; записываем нуль-терминатор
    mov rsi, 1 ; счётчик вписанных в стек символов
    mov rax, rdi ; записываем в аккумулятор число
	
	.loop:
		xor rdx, rdx
		div rbx ; делить на константу нельзя, пишем регистр
		add rdx, 48  ; делаем смещение по таблице ASCII
		dec rsp    ; указатель стека -1
		mov [rsp], dl ; запись в стек остаток от деления
		; остаток от деления помещается в байт, значит
		; можно использовать только один байт - dl, вместо rdx
		inc rsi ; записываем, что количество символов +1
		test rax, rax   ; проверка, что число больше нуля
		jne .loop ; продолжить цикл
	
    mov rdi, rsp ; Записываем в rdi указатель на начало строки
	; так как функция print_string принимает его в rdi
    push rsi ; сохранение количества символов, так как rsi caller-saved и может потеряться
	call print_string
    pop rsi ; восстанавливаем значение rsi
    add rsp, rsi ; возвращаем стек на место
	pop rbx ; восстанавливаем значение rbx
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
global print_int
print_int:
    test rdi, rdi ; сверяем, если число больше нуля
    jge .print ; печатаем сразу, если положительное
    push rdi        ; сохраняем указатель на строку
    mov rdi, 45     ; печатаем символ "-" по таблице ASCII
    call print_char 
    pop rdi         ; восстанавливаем указатель на строку
    neg rdi         ; умножаем число на -1
.print: ; печатаем само число
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
global string_equals
string_equals:
    xor rax, rax ; код возврата по умолчанию 0
    xor rcx, rcx ; итератор i
	push rbx ; сохраняем используемые callee регистры
	push rbp
.loop:
    mov bl, [rdi+rcx] ; i-тый символ первой строки
    mov bpl, [rsi+rcx] ; i-тый символ второй строки
    cmp bl, bpl ; сравниваем символы
    jne .return ; если не равны, то закончить программу. Возвращает 0
    test rbx, rbx ; выставляем флаги по логическому И rbx, rbx (для сравнения с нулём)
    jz .equal ; проверка, что строка кончилась
    inc rcx ; итератор i+1
	jmp .loop
.equal:
    mov rax, 1 ; меняем код возврата на 1, так как строки равны
.return:
	pop rbp ; восстанавливаем callee регистры
	pop rbx 
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
    xor rax, rax ; очищаем аккумулятор, команда sys_read
    push 0 ; готовим место для символа
	
	xor rdi, rdi ; считываем - с stdin
	mov rsi, rsp ; записываем - в стек
	mov rdx, 1 ; количество символов - 1
	
	syscall
	pop rax ; из стека записываем в rax
	ret ; возврат символа в rax
 
 
 
 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
global read_word
read_word:
	xor rax, rax ;  считанный символ, в rdi - адрес буфера, в rsi - длина
    xor rdx, rdx ; длина строки
	dec rsi ; резвервируем место в буфере для нуль-терминатора
	
    push rdx ; сохраняем caller-saved регистры
    push rdi 
    push rsi
	
	.skip_whitespaces: ; пропускаем все пробельные символы
		call read_char
		cmp rax, 0x20
		je .skip_whitespaces
		cmp rax, 0xA
		je .skip_whitespaces
		cmp rax, 0x9
		je .skip_whitespaces
		
	pop rsi ; восстанавливаем caller-saved регистры
	pop rdi
	pop rdx
		
	.loop:
		cmp rsi, rdx ; сверка размера буффера
		jl .too_large_input ; завершить программу если буфер меньше
		
		test rax, rax
		jz .end_of_input ; если на входе 0 - завершить считываение
		
		mov [rdi+rdx], rax ; добавление символа в буффер
		inc rdx
		
		push rdx ; сохраняем caller-saved регистры
		push rdi 
		push rsi
		call read_char
		pop rsi ; восстанавливаем caller-saved регистры
		pop rdi
		pop rdx
		
		cmp rax, 0x20
		je .end_of_input
		cmp rax, 0xA
		je .end_of_input
		cmp rax, 0x9
		je .end_of_input
		
		jmp .loop
		
		
	.end_of_input: ; на этот момент мы знаем, что символы влезают в буфер
		inc rdx 
		mov byte [rdi+rdx], 0
		dec rdx
		mov rax, rdi
		jmp .return
		
	.too_large_input:
		xor rax, rax
	.return:
		inc rsi ; восстанавливаем размер буфера обратно
		ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
global parse_uint
parse_uint:
    xor rax, rax
    xor rsi, rsi
    push rbx
	.loop:
		; Register to match sizes with rax
		xor rbx, rbx
		mov bl, [rdi+rsi]
		
		cmp rbx, '9' ; проверка на соответствие числу
		jg .fail
		cmp rbx, '0' 
		jl .fail
		
		imul rax, 10
		add rax, rbx
		sub rax, '0' ; приводим ASCII к числу
		inc rsi
		jmp .loop
	.fail:
		xor rdx, rdx
	.return:
		mov rdx, rsi
		pop rbx
		ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
global parse_int
parse_int:
    xor r11, r11        ; метка отрицательности
    cmp byte [rdi], '-'     ; обработка минуса
    jne .parse
    not r11             ; запоминаем, что число отрицательное
    inc rdi ; пропуск минуса
	.parse:
		call parse_uint
		test rdx, rdx       ; возврат если число не прочиталось
		jz .return
		test r11, r11       ; число больше нуля, возврат
		jz .return
		neg rax
		inc rdx
	.return:
		ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
global string_copy
string_copy:
	xor rax, rax		; зануляем аккумулятор
	push rbx		; сохраняем callee-saved регистр
	.loop:
		mov bl, byte[rdi+rax]	; копируем символ строки в регистр bl
		mov byte[rsi+rax], bl	; копируем символ из bl в буфер
		dec rdx		; уменьшаем длину буфера на 1
		js .fail		; если после декрементации флаг переноса стал равен 1, то строка не поместилась в буфер -> завершаем рабту подпрограммы
		cmp byte[rdi+rax], 0	; проверяем, не пустой ли байт
		inc rax		; инкрементируем счётчик символов
		jne .loop	; если байт не пустой, то повторяем выполнение цикла
		jmp .end	; иначе завершаем работу программы
	.fail:
		xor rax, rax	; зануляем аккумулятор
	.end:
		pop rbx		; возвращаем регистр rbx
		ret		; завершаем работу подпрограммы


