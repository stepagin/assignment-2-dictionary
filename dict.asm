%include "lib.inc"

global find_word

section .text

; rdi - pointer to the string(key value), rsi - pointer to the first element of the dictionary
; rax - pointer to the value, or 0 if not found
find_word:
.loop_find_word:
    push rsi           ; save dictionary pointer
    push rdi           ; save key pointer
    add rsi, 8         ; move to key value
    call string_equals ; compare key with the current key
    pop rdi            ; restore key pointer
    pop rsi            ; restore dictionary pointer
    test rax, rax      ; if strings are equal
    jne .found_find_word ; jump to .found
    mov rsi, [rsi]     ; get next element
    test rsi, rsi      ; if rsi == 0
    jne .loop_find_word ; jump to .loop
    je .not_found_find_word ; jump to .not_found
.found_find_word:
    mov rax, rsi       ; rax = pointer to the value
    ret
.not_found_find_word:
    xor rax, rax
    ret    