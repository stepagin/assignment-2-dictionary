
bin/app.exe: bin/main.o bin/lib.o bin/dict.o
	ld -o $@ $^

bin/main.o: main.asm dict.asm colon.inc words.inc
	nasm -felf64 -o $@ main.asm

bin/dict.o: dict.asm
	nasm -felf64 -o $@ $<

bin/lib.o: lib.asm
	nasm -felf64 -o $@ $<

.PHONY: clean
clean:
	rm bin/app.exe bin/main.o

.PHONY: run
run:
	make
	bin/app.exe
